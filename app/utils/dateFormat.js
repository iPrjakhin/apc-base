module.exports = class Fateformat {
  constructor() {
    ;
  }

  format(timestamp, form = 'yyyy.mm.dd HH:MM') {
    let date = new Date(Number(timestamp) || Date.now());

    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    let hours = date.getHours();
    let minutes = date.getMinutes();

    return form
      .replace(/yyyy/g, year)
      .replace(/mm/g, month > 9 ? month : '0'+ month)
      .replace(/dd/g, day > 9 ? day : '0'+ day)

      .replace(/HH/g, hours > 9 ? hours : '0'+ hours)
      .replace(/MM/g, minutes > 9 ? minutes : '0'+ minutes);
  }
}
