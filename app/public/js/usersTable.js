class LoadUsers {
  constructor() {
    this.usersList = {};
    this._sort = 'append'; // "append" or "prepend"

    this._load();
  }


  _templateModalForm(userData = {}, nick = '') {console.log(userData);
    let userDataDefault = {
      nick: nick,
      rank: userData.updatelvl && (userData.appendLast < userData.updatelvlLast) ? userData.updatelvl[userData.updatelvlLast] : 'Помощник',
      appendLast: Date.now(),
      info: '',
    };

    userData = $.extend(true, userDataDefault, userData);


    let workTime = 'На посту ';
    if (userData.appendLast > userData.removeLast) workTime += ': '+ this.days(Date.now() - userData.appendLast);
    else workTime += 'пробыл(а): '+ this.days(userData.removeLast - userData.appendLast);


    return `
      <button class="uk-modal-close-default uk-close uk-icon" type="button" uk-close=""></button>

      <div class="uk-modal-header">
        <h2 class="uk-modal-title">${userData.nick || 'Новый пользователь'}</h2>${userData.nick ? workTime +' дней' : ''}
      </div>


      <div class="uk-modal-body" uk-overflow-auto>
        <form class="uk-form-horizontal uk-margin-large" action="?action=save&user=${userData.nick}&table=usersList&" id="userModal_form">
          <div class="uk-margin">
            <label class="uk-form-label" for="userModal_formNick">Изменить ник</label>
            <div class="uk-form-controls">
                <input class="uk-input" id="userModal_formNick" name="userModal_formNick" type="text" placeholder="Ник помощника" value="${userData.nick}">
            </div>
          </div>

          ${userData.nick ? `
            <div class="uk-margin">
              <label class="uk-form-label" for="userModal_formRank">Изменить ранг</label>
              <div class="uk-form-controls">
                  <select class="uk-select" id="userModal_formRank" name="userModal_formRank">
                    <option ${userData.rank == 'Помощник'  ? 'selected="selected"' : ''}>Помощник</option>
                    <option ${userData.rank == 'Мл.модер'  ? 'selected="selected"' : ''}>Мл.модер</option>
                    <option ${userData.rank == 'Модератор' ? 'selected="selected"' : ''}>Модератор</option>
                    <option ${userData.rank == 'Куратор'   ? 'selected="selected"' : ''}>Куратор</option>
                    <option ${userData.rank == 'Власть'    ? 'selected="selected"' : ''}>Власть</option>
                  </select>
              </div>
          </div>
          ` : ''}

          <div class="uk-margin">
            <label class="uk-form-label" for="userModal_formAppend">Изменить дату принятия</label>
            <div class="uk-form-controls">
              <input class="uk-input" id="userModal_formAppend" name="userModal_formAppend" type="date" value="${this.format(userData.appendLast, 'yyyy-mm-dd')}">
            </div>
          </div>

          ${userData.removeLast && userData.removeLast > userData.appendLast ? `
            <div class="uk-margin">
              <label class="uk-form-label" for="userModal_formRemove">Изменить дату изгнания</label>
              <div class="uk-form-controls">
                <input class="uk-input" id="userModal_formRemove" name="userModal_formRemove" type="date" value="${this.format(userData.removeLast, 'yyyy-mm-dd')}">
              </div>
            </div>
          ` : ''}

          <div class="uk-margin">
            <textarea class="uk-textarea" rows="4" name="userModal_formInfo" placeholder="Изменить доп.информацию">${userData.info}</textarea>
          </div>
        </form>
      </div>

      <div class="uk-modal-footer uk-text-right">
        <button type="button" class="uk-button uk-button-primary" id="save">Сохранить</button>
        ${userData.nick ?
          (userData.kicked ?
            '<button type="button" class="uk-button uk-button-secondary" id="back">Вернуть</button> ' :
            '<button type="button" class="uk-button uk-button-secondary" id="kick">Выгнать</button> ')
          + '<button type="button" class="uk-button uk-button-danger" id="del">Удалить</button>' : ''}
      </div>
    `;
  }


  _load() {
    $.getJSON('?action=getAll&table=usersList', data => {
      if (data.error) {
        alert(data.error);
        this.usersList = {};
      }
      else this.usersList = data;

      this._ready();
    });
  }


  _ready() {
    jQuery(document).ready($ => {
      let table = $('#usersTable tbody');
      let userModal = $('#userModal').children().eq(0);
      let slideMenu = $('#slideMenu');


      this._usersTablePaste(table);
      this._usersTableEvent(table, userModal);

      this._slideMenuEvent(userModal, slideMenu);

      this._usersModalEvent(userModal);

      this._loadVersion(slideMenu);
    });
  }


  _loadVersion(slideMenu) {
    $.getJSON('?action=getVersion', data => {
      let version = data.version.split(' ');
      slideMenu.find('> div').append(`<ul class="uk-nav uk-nav-default">v${version[0]} от ${version[1].replace(/\(|\)/g, '')}</ul>`);
    });
  }


  _usersTablePaste(table) {
    Object.keys(this.usersList).forEach((nick, item) => {
      let userData = this.usersList[nick] = JSON.parse(this.usersList[nick]);

      let updatelvlArr = Object.keys(userData.updatelvl || {});
      let complaintArr = Object.keys(userData.complaint || {});

      userData.appendLast = Math.max(...userData.append || []) == '-Infinity' ? 0 : Math.max(...userData.append);
      userData.removeLast = Math.max(...userData.remove || []) == '-Infinity' ? 0 : Math.max(...userData.remove);
      userData.updatelvlLast = +this.arrLast(updatelvlArr);
      userData.complaintLast = +this.arrLast(complaintArr);

      userData.kicked = userData.appendLast < userData.removeLast;


      let rank = '';
      if (userData.updatelvlLast && userData.updatelvlLast > userData.appendLast) rank = userData.updatelvl[userData.updatelvlLast];
      else rank = 'Помощник';

      let updatelvl = ''; console.log(userData.appendLast, this.arrLast(updatelvlArr, 2));
      if (userData.appendLast < userData.updatelvlLast) {
        updatelvl = this.format(userData.updatelvlLast) +' - '+ (userData.appendLast < this.arrLast(updatelvlArr, 2) ? userData.updatelvl[this.arrLast(updatelvlArr, 2)] : 'Помощник') +' &#10148; '+ userData.updatelvl[userData.updatelvlLast];
      }

      let complaint = '';
      if (userData.complaintLast) complaint = `${this.format(userData.complaintLast)} <span uk-tooltip="Кол-во жалоб" class="counter">(${complaintArr.length})</span>`;


      table[this._sort](`
        <tr id="${nick}" uk-toggle="target: #userModal">
          <td>${++item}</td>
          <td>${nick}</td>
          <td>${rank}</td>
          <td>${this.format(userData.appendLast)} <span uk-tooltip="Кол-во раз становления помощником" class="counter">(${userData.append.length})</span></td>
          <td>${userData.kicked ? '<span uk-icon="check" uk-tooltip="Помощник изгнан"></span>' : ''}</td>
          <td>${updatelvl}</td>
          <td>${complaint}</td>
          <td>${userData.info || ''}</td>
        </tr>
      `);
    });
  }


  _usersTableEvent(table, userModal) {
    let self = this;

    table.on('click', 'tr', function() {
      userModal.html(self._templateModalForm(self.usersList[this.id], this.id)); // self.usersList[this.id], this.id

      UIkit.modal('#userModal').show();
    });
  }


  _slideMenuEvent(userModal, slideMenu) {
    let self = this;

    slideMenu.find('li').on('click', 'a', function() {
      switch (this.href.split('#').pop()) {
        case 'add':
          userModal.html(self._templateModalForm());
          UIkit.modal('#userModal').show();
        break;

        default: console.log(`Отсутствует условие для this.href.split('#').pop()="${this.href.split('#').pop()}".`);
      }
    });
  }


  _usersModalEvent(userModal) {
    let self = this;

    userModal.on('click', '.uk-modal-footer > button', function() {
      let nick = $(this).parent().siblings('.uk-modal-header').find('.uk-modal-title').text();
      let userData = self.usersList[nick];

      switch (this.id) {
        case 'save':
          $.get('?'+ userModal_form.action.split('?').pop() + $('#userModal_form').serialize(), data => self._usersModalEvent_result(data));
        break;

        case 'kick':
          if (userData.appendLast < userData.removeLast) return this.notification('Упс...', 'Пользователь уже изгнан', 'warning');

          UIkit.modal.confirm(`Подтвердите изгнание пользователя: <b>${nick}</b>`).then(() =>
            $.get(`?action=kick&table=usersList&user=${nick}`, data => self._usersModalEvent_result(data, nick))
          );
        break;

        case 'back':
          if (userData.appendLast > userData.removeLast) return this.notification('Упс...', 'Пользователь уже возвращен', 'warning');

          UIkit.modal.confirm(`Подтвердите восстановление пользователя: <b>${nick}</b>`).then(() =>
            $.get(`?action=back&table=usersList&user=${nick}`, data => self._usersModalEvent_result(data, nick))
          );
        break;

        case 'del':
          UIkit.modal.confirm(`Подтвердите полное удаление всех данные пользователя: <b>${nick}</b>`).then(() =>
            $.get(`?action=del&table=usersList&user=${nick}`, data => self._usersModalEvent_result(data, nick))
          );
        break;

        default: console.log(`Отсутствует условие для this.id="${this.id}".`);
      }
    });
  }


  _usersModalEvent_result(data, nick) {
    data = JSON.parse(data);

    if (data.error) this.notification('Ошибка', data.error, 'danger');
    else {
      this.notification('Успех', data.success);

      if (nick) $(`#${nick}`).css({'opacity':'0.2', 'pointerEvents':'none'});
    }
  }


  arrLast(arr, count = 1) { return +arr[arr.length - count] || 0; }
  objLast(obj, count = 1) { return this.arrLast(Object.keys(obj), count); }


  days(timestamp) {
    return Math.floor(timestamp / 1000 / 60 / 60 / 24);
  }


  format(timestamp, form = 'yyyy.mm.dd') {
    let date = new Date(Number(timestamp) || Date.now());

    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    let hours = date.getHours();
    let minutes = date.getMinutes();

    return form
      .replace(/yyyy/g, year)
      .replace(/mm/g, month > 9 ? month : '0'+ month)
      .replace(/dd/g, day > 9 ? day : '0'+ day)

      .replace(/HH/g, hours > 9 ? hours : '0'+ hours)
      .replace(/MM/g, minutes > 9 ? minutes : '0'+ minutes);
  }


  notification(title = 'Успех', text = '', status = 'success') {
    UIkit.notification({message:`${title}<br><small>${text}</small>`, status:status, pos:'top-right'});
  }

}



var loadUsers = new LoadUsers();
