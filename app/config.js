class Config {
  constructor() {
    this.ip = '127.0.0.1';
    this.port = '81';

    this.redis = {
      options: {
        host: '127.0.0.1',
        port: '6379',
        base: '0',
        prefix: 'BART96:moderLog:',
      },

      select: 1,

      tables: {
        usersList: 'usersList',
        /*
          "BART96_off" => {
            append: [111, 555],
            remove: [333, 999],

            updatelvl: {123:"Мл.модер"},
            complaint: {123:"Злой"},

            info:"Подозрительный. Следить за ним!",
          }
        */
      },
    };

    this.version = '1.0.1 (20.04.2018)';

  }
}



module.exports = Config;
