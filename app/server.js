/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

'use strict';


const RedisLib = require('redis');
const async = require('async');
const http = require('http');
const path = require('path');
const mime = require('mime-types');
const url = require('url');
const fs = require('fs');
const qs = require('querystring');

const Config = require('./config.js');



class Server {
  constructor() {
    this.config = new Config();

    this.__www = __dirname +'\\public\\';

    this.start();
  }


  start() {
    // >>> Server <<<
    this.server = new http.Server().listen(this.config.port, this.config.ip, err => {
      if (err) return console.log('Server \u27A4', err);
      console.log(`Server \u27A4 Сервер успешно запущен на ip ${this.config.ip}:${this.config.port}`);
      console.log(`Server \u27A4 Версия: v${this.config.version.split(' ')[0]}\n`);
    });

    // >>> Server events <<<
    this.server
      .on('request', (req, res) => this._onRequest(req, res))
      .on('error', err => this._onError(err));

    // >>> Redis <<<
    this.redis = RedisLib.createClient(this.config.redis.options);
    this.redis.select(this.config.redis.select);
    this.redis
      .on('error',   err => console.log('Redis \u27A4', err))
      .on('warning', err => console.log('Redis \u27A4', err))
      .on('ready',   () =>  console.log('Redis \u27A4 ready'));
  }


  sendFile(filePath, res) {
    try {
      filePath = decodeURIComponent(filePath);
    } catch(err) {
      res.writeHead(400, {'Content-type':'text/html; charset=utf-8'});
      res.end('<h1>Ошибка 400</h1>'+ err.message);
      return;
    }

    if (~filePath.indexOf('\0')) {
      res.writeHead(400, {'Content-type':'text/html; charset=utf-8'});
      res.end('<h1>Ошибка 400</h1>Bad Request =\\');
      return;
    }


    filePath = path.normalize(path.join(this.__www, filePath));

    if (filePath.indexOf(this.__www) != 0) {
      res.writeHead(404, {'Content-type':'text/html; charset=utf-8'});
      res.end('<h1>Ошибка 404</h1>Данные не найдены :(');
      return;
    }

    fs.stat(filePath, (err, stats) => {
      if (err) {
        res.writeHead(500, {'Content-type':'text/html; charset=utf-8'});
        res.end('<h1>Ошибка 500</h1>'+ err.message);
        return;
      }

      if (!stats.isFile()) {
        res.writeHead(500, {'Content-type':'text/html; charset=utf-8'});
        res.end('<h1>Ошибка 500</h1>Запрашиваемый путь не является файлом ><');
        return;
      }

      fs.readFile(filePath, (err, content) => {
        if (err) throw err;

        var type = mime.lookup(filePath);
        res.setHeader('Content-Type', type +"; charset=utf-8");
        res.end(content);
      });
    });
  }


  _onRequest(req, res) {
    let getParse = url.parse(req.url, true);
    let pathname = getParse.pathname.endsWith('/') ? getParse.pathname +'index.html' : getParse.pathname;

    if (req.method == 'POST') {
      let body = '';

      req
        .on('data', data => body += data)
        .on('end', () => this._onRequest_post(qs.parse(body)));
    }
    else {
      if (getParse.query.action) this._onRequest_get(getParse.query, res);
      else this.sendFile(pathname, res);
    }
  }


  _onRequest_get(query, res) {
    if (!query.action) return;

    if (query.action == 'getAll' && query.table) {
      this.redis.hgetall(this.config.redis.tables[query.table], (err, data) => {
        if (err) return res.end(JSON.stringify({error:err.message}));

        if (!data || !Object.keys(data)) res.end('{"error":"Данные не найдены :("}');
        else res.end(JSON.stringify(data));
      })
    }

    else if (query.action == 'getVersion') {
      res.end(JSON.stringify({version:this.config.version}));
    }

    else if (query.action == 'save' && query.table) {
      if (query.user) {
        async.waterfall([
          next => this.redis.hget(this.config.redis.tables[query.table], query.user, (err, data) => next(err, data)),
          (data, next) => {
            let userData = JSON.parse(data);

            let appendLast = userData.append.pop();
            let removeLast = userData.remove.pop();
            let updatelvlLast = this.objLast(userData.updatelvl);
            let complaintLast = this.objLast(userData.complaint);


            userData.append.push(this.dateToTimestamp(query.userModal_formAppend));
            if (query.userModal_formRemove) userData.remove.push(this.dateToTimestamp(query.userModal_formRemove));

            if (userData.updatelvl[updatelvlLast] != query.userModal_formRank) userData.updatelvl[Date.now()] = query.userModal_formRank;

            userData.info = query.userModal_formInfo || '';

            next(null, JSON.stringify(userData));
          },
          (data, next) => {
            if (query.user == query.userModal_formNick) next(null, data, undefined);
            else this.redis.hexists(this.config.redis.tables[query.table], query.userModal_formNick, (err, check) => next(err, data, check));
          },
          (data, check, next) => {
            if (check) return next(`Пользователь с ником "${query.userModal_formNick}" уже существует`);

            this.redis.hdel(this.config.redis.tables[query.table], query.user, (err) => next(err, data));
          },
          (data, next) => this.redis.hset(this.config.redis.tables[query.table], query.userModal_formNick, data, (err, data) => next(err, `Пользователь с ником "${query.userModal_formNick}" обновлен`)),

        ], (err, result) => {
          if (err) return res.end(JSON.stringify({error:err}));
          res.end(JSON.stringify({success:result}));
        });
      }

      else if (query.userModal_formNick) {
        this.redis.hexists(this.config.redis.tables[query.table], query.userModal_formNick, (err, check) => {
          if (err) return res.end(JSON.stringify({error:err.message}));
          if (check) return res.end(`{"error":"Пользователь с ником \\"${query.userModal_formNick}\\" уже существует"}`);

          this.redis.hset(this.config.redis.tables[query.table], query.userModal_formNick, JSON.stringify({
            append: [this.dateToTimestamp(query.userModal_formAppend)],
            remove: [],
            updatelvl: {},
            complaint: {},
            info: query.userModal_formInfo || '',
          }), (err, data) => {
            if (err) return res.end(JSON.stringify({error:err.message}));

            res.end(`{"success":"Пользователь \\"${query.userModal_formNick}\\" добавлен"}`);
          });
        });
      }

      else {
        res.end(`{"error":"Не хватает минимальных данных"}`);
      }
    }

    else if (query.action == 'kick' || query.action == 'back' && query.table && query.user) {
      this.redis.hget(this.config.redis.tables[query.table], query.user, (err, data) => {
        if (err) return res.end(JSON.stringify({error:err.message}));

        let userData = JSON.parse(data);
        userData[query.action == 'kick' ? 'remove' : 'append'].push(Date.now());

        this.redis.hset(this.config.redis.tables[query.table], query.user, JSON.stringify(userData), (err, data) => {
          if (err) return res.end(JSON.stringify({error:err.message}));

          res.end(`{"success":"Пользователь \\"${query.user}\\" ${query.action == 'kick' ? 'изгнан' : 'восстановлен'}"}`);
        });
      });
    }

    else if (query.action == 'del' && query.table && query.user) {
      this.redis.hdel(this.config.redis.tables[query.table], query.user, (err, data) => {
        if (err) return res.end(JSON.stringify({error:err.message}));

        res.end(`{"success":"Пользователь \\"${query.user}\\" удален из базы \\"${query.table}\\""}`);
      });
    }
  }


  objLast(obj, count = 1) {
    let arr = Object.keys(obj);
    return arr[arr.length - count] || 0;
  }


  dateToTimestamp(date) {
    let dateArr = date.split("-");
    return new Date(`${dateArr[1]}/${dateArr[2]}/${dateArr[0]}`).getTime();
  }


  _onRequest_post(query) {
    if (!query.action) return;
  }


  _onError(err) {
    process.exit(console.log('Server \u27A4', err));
  }

}



module.exports = Server;
