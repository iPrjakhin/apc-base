/* Автор - Пряхин Игорь  ::  BART96  ::  Author - Prjakhin Igor */
/* Уважайте чужой труд.  ::  Respect other peoples work. */

'use strict';

if (Number(process.version.slice(1).split('.')[0]) < 9) throw new Error('Node 9.0.0 or higher is required. Update Node on your system.');

let Server = require('./app/server.js');
let server = new Server();
